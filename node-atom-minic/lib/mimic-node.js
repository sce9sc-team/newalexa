//Mimic Test
//https://github.com/MycroftAI/mimic
// Voices kal awb_time kal16 awb rms slt ap 

var execa = require('child_process').spawn;
var EventEmitter = require('events');

class Mimic extends EventEmitter{
	constructor(params){
		super();
		this.cmd = '../mimic/bin/mimic';
		this.cmdArgs = [
            '-t',                     // show no progress
           	'Hi, my name is Alexa',
           	'-voice',
           	'slt'
        ];
		this.cp = null
		this.running = false;
	}

	speak(text){
		if(!this.running){
			this.running = true;
			this.cmdArgs[1] = text || "How can I help you"; 
			this.cp = execa(this.cmd, this.cmdArgs, { encoding: 'binary' })
			this.std = this.cp.stdout;
	     	this.std.on('end',() =>{
	     		console.log('end')
	     		this.cp.kill()
	     		this.running = false;
	     		this.emit('end');
	  		});

		}

	}
}



module.exports = Mimic;


