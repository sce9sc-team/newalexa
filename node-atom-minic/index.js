var Mimic = require('./lib/mimic-node');

let instance = null
module.exports = function(){
    if(instance){
        return instance;
    }
    instance = new Mimic();
    return instance;
}