__author__ = 'the-kid89'
"""
A sample program that uses multiple intents and disambiguates by
intent confidence
try with the following:
PYTHONPATH=. python examples/multi_intent_parser.py "what's the weather like in tokyo"
PYTHONPATH=. python examples/multi_intent_parser.py "play some music by the clash"
"""

import json
import sys
#sys.path.append('/Users/sce9sc/Documents/Work/mic_record/adapt')
from adapt.entity_tagger import EntityTagger
from adapt.tools.text.tokenizer import EnglishTokenizer
from adapt.tools.text.trie import Trie
from adapt.intent import IntentBuilder
from adapt.parser import Parser
from adapt.engine import DomainIntentDeterminationEngine


tokenizer = EnglishTokenizer()
trie = Trie()
tagger = EntityTagger(trie, tokenizer)
parser = Parser(tokenizer, tagger)

engine = DomainIntentDeterminationEngine()

engine.register_domain('Domain1')
engine.register_domain('Domain2')

# define vocabulary
weather_keyword = [
    "weather"
]

for wk in weather_keyword:
    engine.register_entity(wk, "WeatherKeyword", domain='Domain1')

weather_types = [
    "snow",
    "rain",
    "wind",
    "sleet",
    "sun"
]

for wt in weather_types:
    engine.register_entity(wt, "WeatherType", domain='Domain1')

locations = [
    "Seattle",
    "San Francisco",
    "Tokyo",
    "Greece",
    "Athens"
]

for l in locations:
    engine.register_entity(l, "Location", domain='Domain1')

# structure intent
weather_intent = IntentBuilder("WeatherIntent")\
    .require("WeatherKeyword")\
    .optionally("WeatherType")\
    .require("Location")\
    .build()


# define music vocabulary
artists = [
    "third eye blind",
    "the who",
    "the clash",
    "john mayer",
    "kings of leon",
    "adelle"
]

for a in artists:
    engine.register_entity(a, "Artist", domain='Domain2')

music_verbs = [
    "listen",
    "hear",
    "play"
]

for mv in music_verbs:
    engine.register_entity(mv, "MusicVerb", domain='Domain2')

music_keywords = [
    "songs",
    "music"
]

for mk in music_keywords:
    engine.register_entity(mk, "MusicKeyword", domain='Domain2')

music_intent = IntentBuilder("MusicIntent")\
    .require("MusicVerb")\
    .optionally("MusicKeyword")\
    .optionally("Artist")\
    .build()



#TV Intent======================
# define appliance vocabulary
tvvoc1= [
    "on",
    "off",
    "channel",
    "mode",
    "volume"
]

for tvc1 in tvvoc1:
    engine.register_entity(tvc1, "TvCommands", domain='Domain4')

tvvoc2= [
    "up",
    "down",
    "left",
    "right",
    "next",
    "previous"
]

for tvc2 in tvvoc2:
    engine.register_entity(tvc2, "TvSubCommands", domain='Domain4')


tvlocation= [
    "room",
    "kitchen",
    "living",
]

for tvloc in tvlocation:
    engine.register_entity(tvloc, "TvLocation", domain='Domain4')


tv_verbs = [
    "switch",
    "open",
    "close",
    "turn",
    "change",
    "increase",
    "decrease",
    "mute"
]

for tverbs in tv_verbs:
    engine.register_entity(tverbs, "TvVerb", domain='Domain4')

tv_keywords = [
    "tv",
    "television"
]

for tk in tv_keywords:
    engine.register_entity(tk, "TvKeyword", domain='Domain4')

tv_intent = IntentBuilder("TvIntent")\
    .require("TvKeyword")\
    .optionally("TvVerb")\
    .optionally("TvCommands")\
    .optionally("TvSubCommands")\
    .optionally("TvLocation")\
    .build()

#TV Intent======================END

#Lights Intent======================
# define appliance vocabulary
lightsvoc1= [
    "on",
    "off",
   
]

for lsv1 in lightsvoc1:
    engine.register_entity(lsv1, "LightCommand", domain='Domain5')


lightvoc= [
    "first",
    "one",
    "second",
    "two",
    "third"
    "three",
    "forth",
    "four",
    "fifth",
    "five",
    "sixth",
    "six"
   
]

for lv1 in lightvoc:
    engine.register_entity(lv1, "LightNumber", domain='Domain5')

lightslocation= [
    "room",
    "kitchen",
    "living",
]

for ll in lightslocation:
    engine.register_entity(ll, "LightLocation", domain='Domain5')



lights_verbs = [
    "switch",
    "open",
    "close",
    "turn",
    "change",
    "increase",
    "decrease",
    ]

for lsv in lights_verbs:
    engine.register_entity(lsv, "LightVerb", domain='Domain5')

lights_keywords = [
    "light",
    "lights"
]

for lsk in lights_keywords:
    engine.register_entity(lsk, "LightKeyword", domain='Domain5')

lights_intent = IntentBuilder("LightIntent")\
    .require("LightKeyword")\
    .optionally("LightVerb")\
    .optionally("LightCommand")\
    .optionally("LightLocation")\
    .optionally("LightNumber")\
    .build()

#Lights Intent======================END




engine.register_intent_parser(weather_intent, domain='Domain1')
engine.register_intent_parser(music_intent, domain='Domain2')
#engine.register_intent_parser(apliance_intent, domain='Domain3')
engine.register_intent_parser(tv_intent, domain='Domain4')
engine.register_intent_parser(lights_intent, domain='Domain5')



if __name__ == "__main__":
    stack = []
    for intents in engine.determine_intent(' '.join(sys.argv[1:])):
        stack.append(json.dumps(intents, indent=None))

    print(json.dumps(stack))

