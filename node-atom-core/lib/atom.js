var fs = require('fs');
var stream = require('stream');
var repl = require("repl");

var request = require('request');
var beep = require('beepbeep');
var async = require('async');
const {Detector, Models} = require('snowboy');

var micStream = require('../../node-atom-lcpm16');
var say = require('say')//require('node-atom-mimic');
var adapt = require('../../node-atom-adapt');
var config = require('../config')
var speechApi = require('../../node-atom-mstt')(config);
var BroadlinkService = require('../../node-atom-broadlink')
var intents = require('../../node-atom-intents')

class Atom {
  constructor(){
      this.streamMic =null
      this.broadlink = new BroadlinkService();
      this.adapt = new adapt();
      this.adapt.on("data",(data)=>{this.processCmd1(data)});
      this.speechToText = "";

  }

  createDetector(){
  	let models = new Models();
   	models.add({
      file: 'resources/Hey_Atom.pmdl',
      sensitivity: '0.45',
      hotwords : 'hey Atom'
    });

  	let detector = new Detector({
      resource: "resources/common.res",
      models: models,
      audioGain: 1.9
    });

    detector.on('silence', ()=>{
     console.log('silence');
    });

    detector.on('sound', function () {
      console.log('sound');
    });

    detector.on('error', function () {
      console.log('error');
    });

    detector.on('hotword', (index, hotword)=> {
      console.log('hotword', index, hotword);
      this.stopReco();
     
     setTimeout(()=>{
      	console.log('start rec======')
      	beep()
  		this.sendReco()
        },1000)

     setTimeout(()=>{
        	console.log('stop rec ======')
  		beep()
  		this.stopReco()
        },4000)
        
      setTimeout(()=>{
        	console.log('start detect======')
  		this.start()
        },6100)
	
     });

    return detector
  }

  stopReco(){
	console.log('stopp')
  	micStream.stop()
  	this.streamMic.emit('end')
    	this.streamMic.unpipe()

  }

  speak(text){
  	 say.speak(text);
  }

  processCmd1(data){
  	//"confidence": 0.75, "target": null, "intent_type": "AplianceIntent", 
  	//"AplianceVerb": "turn", "AplianceKeyword": "tv", "ApliancesStates": "off"}
  	
  	if(data.length){
  		var pData = JSON.parse(data);
	  	console.log(pData)
	  	if(pData.intent_type!=undefined){
	  		//found intent
			if(intents[pData.intent_type]!=undefined){
				intents[pData.intent_type](pData,this);
			}else{
				console.log('no intent')
			}
  		}
  	}else{
  		var textres = ""
  		var text = this.speechToText
  		switch(text){
			case "what is your name?":
				textres = 'My name is Alexa';
				break;
			case "how are you?":
				textres = 'I am fine thank you master';
				break;
			case "who am i?":
				textres = 'You are Stavros the master';
				break;
			case "tell me the time.":  
			case "what's the time?":  
			case "what time is it?":
				var date =new Date();
				var time = (date.getHours()>12?date.getHours()-12:date.getHours()) + ' and ' + date.getMinutes() + ' minutes';
				textres = 'The time is' +time;
			break;
			default:
				textres ='Sorry did not understand master';
				break;
		}
		console.log(textres);
		//this.speak(textres);
  	}
  }

  sendReco(){
  	// this is for Microsoft cognitive services
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})

  	speechApi.getTextFromAudioStream(this.streamMic).then((res)=>{
  		if(res!=undefined){
  			var txtRec = res.toLowerCase();
  			this.speechToText = txtRec;
  			this.adapt.parse(txtRec);
  		}else{
  			this.speechToText = ""
  			//console.log('error nothing was recognized');
  		}
  	}).catch((err)=>{
  		console.log(err)
  	})
  }

  start(){ 
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})
    this.detector = this.createDetector();
    this.streamMic.pipe(this.detector)
  }

}

module.exports = Atom;
