var Atom = require('./lib/atom')
var repl = require('repl')

var app = new Atom();
app.start()

var replServer = repl.start({
    prompt: "home-auto > ",
});
replServer.context.server = app;
