module.exports = function(data,atom){
    /*
     . IntentBuilder("LightIntent")\
     .require("LightKeyword")\
     .optionally("LightVerb")\
     .optionally("LightCommand")\
     .optionally("LightLocation")\
     .optionally("LightNumber")\
     */
    var LightVerb = data.LightVerb;
    var LightCommand = data.LightCommand;
    var LightLocation = data.LightLocation;
    var LightNumber = data.LightNumber;
    var LightKeyword = data.LightKeyword;
    if(LightKeyword=="light")
    {
        var deviceName = 'light_'+LightNumber+'_'+LightLocation;
        var cmd = null;

        if(LightVerb === "open" ){
            LightCommand = "on"
        }
        if(LightVerb === "close" ){
            LightCommand = "off"
        }

        cmd = LightCommand;

        var command = {
            device:deviceName,
            command:cmd
        }

        console.log('sending Command',JSON.stringify(command))

        atom.broadlink.devices["46-203-251-13-67-180"].send_data(command)
        atom.speak("light")
    }else{
        //issue a group command delayed by ms

        if(LightVerb === "open" ){
            LightCommand = "on"
        }
        if(LightVerb === "close" ){
            LightCommand = "off"
        }
        var deviceControllersbyGroup = atom.broadlink.getDevicesByGroup('lights');
        var deviceNames = Object.keys(deviceControllersbyGroup);
        var commandsToBeProcessed = []
        deviceNames.forEach((dname)=>{

            var controllers = deviceControllersbyGroup[dname]
            controllers.forEach((cName)=>{
                var cmd = {
                    device:cName,
                    command:LightCommand
                }
                var broadlink = atom.broadlink
                var cfn = function(cmd){
                    return function(clb){
                        broadlink.devices[dname].send_data(cmd)
                        setTimeout(function(){clb()},400)
                    }
                }(cmd)

                commandsToBeProcessed.push(cfn)
            })


        })
        console.log(commandsToBeProcessed);
        if(commandsToBeProcessed.length){
            async.waterfall(commandsToBeProcessed, function (err, result) {
                // result now equals 'done'
                console.log('lights finished')
            });
        }else{
            console.log('no group commands')
        }

    }

}