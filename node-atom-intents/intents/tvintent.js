module.exports = function(data,atom){
    /*
     .require("TvKeyword")\
     .optionally("TvVerb")\
     .optionally("TvCommands")\
     .optionally("TvSubCommands")\
     .optionally("TvLocation")\
     */
    var TvVerb = data.TvVerb;
    var TvCommand = data.TvCommands;
    var TvSubCommand = data.TvSubCommands;
    var TvLocation = data.TvLocation;
    var deviceName = 'tv_'+TvLocation;
    var cmd = null;

    if(TvVerb === "open" ){
        TvCommand = "on"
    }
    if(TvVerb === "close" ){
        TvCommand = "off"
    }

    cmd = TvCommand;

    if(TvSubCommand!==undefined){
        cmd = {}
        cmd[TvSubCommand]=TvCommand
    }



    var command = {
        device:deviceName,
        command:cmd
    }

    console.log('sending Command',JSON.stringify(command))

    atom.broadlink.devices["46-203-251-13-67-180"].send_data(command)
    atom.speak("tv")

}