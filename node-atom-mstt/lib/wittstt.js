const request = require("request");

class WITSTT {
    constructor(params){
        this.witToken = params.witToken || "";
    }

    getTextFromAudioStream(stream){
        return new Promise(
            (resolve, reject) => {
                this.streamToText(stream, resolve, reject);
            }
        );

    }

    streamToText(stream, resolve, reject){
        const speechApiUrl = 'https://api.wit.ai/speech?v=20160526'

        const speechRequestData = {
            url: speechApiUrl,
            headers: {
                'Accept'        : 'application/vnd.wit.20160202+json',
                'Authorization' : 'Bearer ' + this.witToken,
                'Content-Type'  : 'audio/wav'
            }
        }

        stream.pipe(request.post(speechRequestData, (error, response, body) => {
            if (error) {
                reject(error);
            }
            else if (response.statusCode != 200) {
                reject(body);
            }
            else {
                resolve(JSON.parse(body).header.name);
            }
        }));

    }
}

module.exports = WITSTT
