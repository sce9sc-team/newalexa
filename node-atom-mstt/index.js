const MTTS = require('./lib/mstt');

let instance =null;

//Singleton
module.exports = function(params){
    if(instance){
        return instance
    }
    instance = new MTTS(params)
    return instance
}